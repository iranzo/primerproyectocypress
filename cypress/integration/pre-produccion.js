describe('test pre producción', () => {
    beforeEach(() => {
        cy.visit('https://preaplicaciones.aragon.es/geb/')
        cy.get('#processLogin_idUsuario').type('jiranzo');
        cy.get('#processLogin_password').type('test');
        cy.contains('Conectar').click();
        cy.contains('EPACF_ADM - Actos de Elección de Funcionarios').should('exist').click();
    })
    // compruebo la versión del programa con la versión que tiene que tener
    it('La versión del programa', () => {
        // captura de la versión del programa
        cy.contains('Versión 1.0.13').screenshot('version');
    })
    it('Añadir, modificar y borrar candidato colectivo 4 - 5 - Sin destino definitivo', () => {
        /* ==== Generated with Cypress Studio ==== */
        cy.log('**AÑADIR CANDIDATO**')
        // AÑADIR CANDIDATO
        cy.get(':nth-child(2) > .textoApl1 > a').click();
        cy.get(':nth-child(8) > a > .sinBorde').click();
        cy.get(':nth-child(6) > a').click();
        cy.contains('Añadir Candidato').click();
        cy.get('#actProcessAgnadirActoCandidatoColectivo_idColectivo').select('4');
        cy.get('#actProcessAgnadirActoCandidatoColectivo_nif').clear();
        cy.get('#actProcessAgnadirActoCandidatoColectivo_nif').type('17703071V');
        cy.get('#Aceptar').click();
        cy.contains('Añadir Candidato - Colectivo 4 - 5 - Sin destino definitivo')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_gestor_id').select('50');
        // comprobando el año de ingreso
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('El año de ingreso debe ser un valor numérico')
        cy.contains('El año de ingreso debe ser un valor numérico')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('1960')
        // comprobando el orden de ingreso
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('El orden de ingreso debe ser un valor numérico')
        cy.contains('El orden de ingreso debe ser un valor numérico')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('1234')
        // comprobando la nota de ingreso
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_notaIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('La nota de ingreso no es válida')
        cy.contains('La nota de ingreso no es válida')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_notaIngreso').clear().type('1.2345')
        // comprobando la puntuación del concurso de traslados
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_puntuacion').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('La puntuación introducida no es válida')
        cy.contains('La puntuación introducida no es válida')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_puntuacion').clear().type('5.4321')
        cy.get('#Aceptar').click();
        cy.log('**MODIFICAR CANDIDATO**')
        // MODIFICAR CANDIDATO  
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_nif').clear().type('17703071V');
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_idColectivo').select('4');
        cy.get('#Buscar').click();
        cy.get(':nth-child(10) > a > .sinBorde').click();
        cy.contains('Modificar Candidato - Colectivo 4 - 5 - Sin destino definitivo')
        // comprobando el año de ingreso
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('El año de ingreso debe ser un valor numérico')
        cy.contains('El año de ingreso debe ser un valor numérico')
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('1960')
        // comprobando el orden de ingreso
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('El orden de ingreso debe ser un valor numérico')
        cy.contains('El orden de ingreso debe ser un valor numérico')
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('1234')
        // comprobando la nota de ingreso
        cy.get('#actProcessModificarActoCandidatoColectivo4_notaIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        // cy.screenshot('La nota de ingreso no es válida')
        cy.contains('La nota de ingreso no es válida')
        cy.get('#actProcessModificarActoCandidatoColectivo4_notaIngreso').clear().type('6.789')
        // comprobando la puntuación del concurso de traslados
        cy.get('#actProcessModificarActoCandidatoColectivo4_puntuacion').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.contains('La puntuación introducida no es válida')
        cy.get('#actProcessModificarActoCandidatoColectivo4_puntuacion').clear().type('0.1234')
        cy.get('#Aceptar').click();

        cy.log('**BORRAR EL CANDIDATO**')
        // BORRAR EL CANDIDATO
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_nif').clear().type('17703071V');
        cy.get('#Buscar').click();
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_idColectivo').select('4');
        cy.get('#Buscar').click();
        cy.get(':nth-child(12) > a > .sinBorde').click();
        cy.get('#Aceptar').click();
        cy.get('.actionMessage').click();
        cy.contains('El candidato se ha eliminado correctamente')
        /* ==== End Cypress Studio ==== */
    })

    // al finalizar cada uno de los tests
    afterEach(() => {
        cy.get('a[href*="logout.action"]').click()
    })
})
