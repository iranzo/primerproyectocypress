// desarrollo.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
describe('test desarrollo', () => {
    it('primer test', () => {
        cy.visit('localhost:8080/eppla_adm/start.action?idUsuario=jiranzo&idAplicacion=54&token=1')
        /* ==== Generated with Cypress Studio ==== */
        cy.get('[d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"]').click({ force: true });
        cy.get('#plaProcessBuscarPlantillaCentros_formBusqueda_codCentro').clear();
        cy.get('#plaProcessBuscarPlantillaCentros_formBusqueda_codCentro').type('50000126');
        cy.get('#Buscar').click();
        cy.get('[title="Modificar centro"] > .feather-padding > .feather').click();
        cy.get(':nth-child(1) > :nth-child(11) > [title="Modificar especialidad del centro"] > .feather-padding > .feather').click();
        cy.get(':nth-child(13) > .container-fluid > :nth-child(1) > .card-body > .row').click();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').clear();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').type('JAVIER');
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').clear();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').type('JAVIER');
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').clear();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').type('JAVIER');
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').clear();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').type('JAVIER');
        cy.get(':nth-child(14) > .container-fluid').click();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaCentroEspecialidad_observaciones').clear();
        cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaCentroEspecialidad_observaciones').type('OBSERVACIONES DE LA PLANTILLA DEL CURSO 2021 PARA EL CENTRO 50000126 Y LA ESPECIALIDAD 0597 - AL CAMBIADAS');
        cy.get('#Aceptar').click();
        /* ==== End Cypress Studio ==== */

        // validando los campos con formato no válido
        cy.contains('El campo Plantilla Jurídica Curso Próximo tiene un formato no válido').should('exist');
        cy.contains('El campo Ocupación Jubilación tiene un formato no válido').should('exist');
        cy.contains('El campo Ocupación Reserva Negativa tiene un formato no válido').should('exist');
        cy.contains('El campo Ocupación Reserva Vacante tiene un formato no válido').should('exist');

           // validando los campos con valor núlo
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').clear();

    cy.get('#Aceptar').click();

    cy.log('**PROBANDO LA VALIDACIÓN DE LOS CAMPOS DESDE LAS PROPIEDADES DEL HTML**')

    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })

    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })

    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })

    // valores validos
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').clear().type('20');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').clear().type('40');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').clear().type('50');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').clear().type('60');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaCentroEspecialidad_observaciones').clear().type('OBSERVACIONES DE LA PLANTILLA DEL CURSO 2021 PARA EL CENTRO 50000126 Y LA ESPECIALIDAD 0597 - AL CAMBIADAS');

    cy.get('#Aceptar').click();

    cy.contains('La operación se ha realizado correctamente').should('exist')

    // comprobando los valores introducidos

    cy.get(':nth-child(1) > :nth-child(11) > [title="Modificar especialidad del centro"]').click({ force: true });
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').invoke('val').should('eq', '20');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').invoke('val').should('eq', '40');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').invoke('val').should('eq', '50');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').invoke('val').should('eq', '60');
    })
})
