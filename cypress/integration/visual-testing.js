describe('test visual', () =>
{
    it('comprobando la pantalla inicial de forma visual', () =>
    {
        cy.visit('..//pruebas/paddoc.html')
        cy.get('body').matchImageSnapshot({
            imageConfig: {
                threshold: 0.001,
            },
        })
    })
})
