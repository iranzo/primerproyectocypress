# PRIMER PROYECTO CYPRESS

![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
![Prerequisite](https://img.shields.io/badge/npm-%3E%3D10.0-blue.svg)
![cypress version](https://img.shields.io/badge/cypress-7.6.0-brightgreen)

* primer proyecto en cypress para seguir el [taller-practico-cypress en bitbucket](https://bitbucket.org/javieriranzo3/taller-practico-cypress/src/master/)

# INSTALACIÓN

```bash
git clone https://javieriranzo3@bitbucket.org/javieriranzo3/primerproyectocypress.git
cd primerproyectocypress
npm install
```

# LANZAR LOS TESTS

* se pueden lanzar con

```bash
# para abrir Cypress
npm run cy:open
# para lanzar el primer test
npm run cy:run
# para lanzar test en pre producción
npm run cy:run-pre
# para lanzar el test visual
npm run cy:run-visual
```


# test visual

* instalar el plugin [cypress-image-snapshot](https://www.npmjs.com/package/cypress-image-snapshot)

```bash
npm install --save-dev cypress-image-snapshot
```

* en el archivo package.json

```js
"devDependencies": {
"cypress": "^7.6.0",
"cypress-image-snapshot": "^4.0.1"
}
```

* una vez instalado el plugin tenemos que añadir al final de este archivo  `C:\proyectos\primerproyectocypress\cypress\plugins\index.js` el siguiente código

```javascript
const {
  addMatchImageSnapshotPlugin,
} = require('cypress-image-snapshot/plugin');

module.exports = (on, config) => {
  addMatchImageSnapshotPlugin(on, config);
};
```

* y en `C:\proyectos\primerproyectocypress\cypress\support\commands.js`

```javascript
import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand();
```

* esto nos permite añadir en nuestro test esta instrucción

```javascript
cy.get('body').matchImageSnapshot({
    imageConfig: {
        threshold: 0.001,
    },
})
```      

* el test crea una carpeta para las instantáneas `snapshots` con la captura de la parte del DOM que hemos indicado que en este caso es todo el `body`


     Error: Image was 1.8776792608568311% different from saved snapshot with 14104 different pixels.
See diff for details: C:\proyectos\cypress\primerproyectocypress\cypress\snapshots\visual-testing.js\__diff_output__\test visual -- comprobando la pantalla inicial de forma visual.diff.png
